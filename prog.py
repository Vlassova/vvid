import math


def input_check(length, rads):
    """Функция проверки ввода.
     Принимает на вход введенные пользователем значения.
    Args:
        length: первый входной аргумент.
        rads:   второй входной аргумент.
    Returns:
      Введенные значения
    Raises:
        TypeError
        ValueError

    Examples:
        >>> input_check('shsjshjh','2')
        Traceback (most recent call last):
        ...
        TypeError: rads and length should be positive numbers
        >>> input_check('1','2')
        Traceback (most recent call last):
        ...
        ValueError: rads should be < length
        >>> input_check('0','0')
        Traceback (most recent call last):
        ...
        ValueError: rads should be < length
        >>> input_check('4','2')
        (4, 2)"""

    if not (rads.isnumeric() and length.isnumeric()):
        raise TypeError('rads and length should be positive numbers')
    rads = int(rads)
    length = int(length)
    if rads >= length or rads == 0:
        raise ValueError('rads should be < length')
    return length, rads


def lamp_num(length, rads):
    """Функция принимает на вход два целых положительных числа:  радиус освещения фонаря и длинну дороги.
     Вычисляет минимальное число фонарей, которые освещают всю дорогу.
     Args:
        length: первое число.
        rads:   второе число.
    Returns:
        lnum: число фонарей.
    Examples:
        >>> lamp_num(4, 3)
        1
        >>> lamp_num(8,4)
        1
        >>> lamp_num(17, 4)
        3

    """
    lnum = math.ceil(length / (2 * rads))
    return lnum


def main():
    """Функция выводит минимальное число фонарей."""
    while True:
        rads = input('enter the radius: ')
        length = input('enter the length of the road: ')
        try:
            length, rads = input_check(length, rads)
        except (ValueError, TypeError):
            print('wrong input')
        else:
            break
    lnum = lamp_num(length, rads)
    print('minimum number of lights = {}'.format(lnum))


if __name__ == "__main__":
    main()
