from prog import input_check, lamp_num
import pytest


def test_numbers_input_check():
    """тест на нормальные числа"""
    assert input_check('3', '2') == (3, 2)


def test_words_input_check():
    """тест на буквы"""
    with pytest.raises(TypeError):
        input_check('acd', '3')


def test_symbols_input_check():
    """тест на спец.символы"""
    with pytest.raises(TypeError):
        input_check('2', '!')


def test_neg_num_input_check():
    """тест на отрицательные числа"""
    with pytest.raises(TypeError):
        input_check('-2', '-1')


def test_wrong_num_input_check():
    """ тест на неверную длину дороги"""
    with pytest.raises(ValueError):
        input_check('1', '2')


def test_zero_num_input_check():
    """тест на нулевые числа"""
    with pytest.raises(ValueError):
        input_check('0', '0')


def test_wrong_num2_input_check():
    """тест на нулевой радиус"""
    with pytest.raises(ValueError):
        input_check('1', '0')


def test_wrong_num3_input_check():
    """тест на нулевую длину дороги"""
    with pytest.raises(ValueError):
        input_check('0', '1')


def test1_lamp_num():
    """тест  №1
    длина дороги - 4
    радиус освещения фонаря - 2"""
    assert lamp_num(4, 2) == 1


def test2_lamp_num():
    """тест  №2
    длина дороги - 15
    радиус освещения фонаря - 3"""
    assert lamp_num(15, 3) == 3


def test3_lamp_num():
    """тест  №3
    длина дороги - 6
    радиус освещения фонаря - 5"""
    assert lamp_num(6, 5) == 1
